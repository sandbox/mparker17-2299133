<?php

/**
 * @file
 * Displays a single CSS palette swatch.
 *
 * Available variables:
 * - $class: The CSS class to apply to the color swatch.
 * - $name: The translated, human-readable name of the color swatch.
 *
 * @ingroup themeable
 */
?>
<div class="styleguide-palette-css-swatch">
  <div class="styleguide-palette-css-swatch-color <?php print $class; ?>">&nbsp;</div>
  <h3 class="styleguide-palette-css-swatch-name"><?php print $name; ?></h3>
</div>
