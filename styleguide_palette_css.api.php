<?php

/**
 * @file
 * How to use the styleguide_palette_css module.
 */

/**
 * Implements hook_styleguide().
 *
 * Note this is never run as this file is not included at execution-time.
 */
function styleguide_palette_css_styleguide() {
  $items = array();

  // Example group of background colors.
  $items['bg_colors'] = array(
    'group' => t('Example colors'),
    'title' => t('Background colors'),
    'theme' => 'styleguide_palette_css_colors',
    'variables' => array(
      'classes_colors' => array(
        'bg_class_to_apply' => t('Color name'),
      ),
    ),
  );

  // Example group of foreground colors.
  $items['fg_colors'] = array(
    'group' => t('Example colors'),
    'title' => t('Foreground colors'),
    'theme' => 'styleguide_palette_css_colors',
    'variables' => array(
      'classes_colors' => array(
        'fg_class_to_apply' => t('Color name'),
      ),
    ),
  );

  return $items;
}
