CONTENTS OF THIS FILE
---------------------

* Overview
* Features
* Requirements
* Installation
* Known problems
* Version history
* Future plans
* Similar projects
* Credits

OVERVIEW
--------

Drupal's Style guide module (https://www.drupal.org/project/styleguide) contains
a *Style guide palette* sub-module, which stores color swatches in the database
and displays them on a separate page from the main style guide. However, themers
who develop with CSS pre-processors like SCSS or LESS often store colors as
variables inside the stylesheets themselves, and these colors may change as the
project evolves (meaning that a database update would need to happen each time a
color changes).

If you'd prefer to store your colors only in the CSS, this module is for you!
Style guide: CSS palette allows themers to add color swatches to the styleguide
by implementing `hook_styleguide()` and associating class names with color
names, then adding a few lines of CSS to your theme.

FEATURES
--------

* Add, edit and remove colors without deploying database updates using
  `hook_styleguide()` and CSS.

REQUIREMENTS
------------

* The Style guide module (https://www.drupal.org/project/styleguide).

INSTALLATION
------------

1. Download, install and enable the styleguide_palette_css project.

   See https://drupal.org/node/895232 for further information.

2. Create a very simple module (just a .info and empty .module file) to extend
   the styleguide.

   A recommended way to name this new module is to take the machine name of the
   theme and add `_styleguide` to the end. For example, if your theme is named
   `sitename2014`, a good name for the new module would be
   `sitename2014_styleguide`.

   See https://drupal.org/node/361112 and https://drupal.org/node/1095546 for
   further information.

3. In the module, implement `hook_styleguide()` as follows:

       function hook_styleguide() {
         $items = array();

         // Example group of background colors.
         $items['bg_colors'] = array(
           'group' => t('Example colors'),
           'title' => t('Background colors'),
           'theme' => 'styleguide_palette_css_colors',
           'variables' => array('classes_colors' => array(
             'bg_class_to_apply' => t('Color name'),
           )),
         );

         return $items;
       }

   Add a CSS-class/human-readable-name pair to the `classes_colors` array for
   each color you want to include in the style guide.

   See the documentation for `hook_styleguide()` for further information.

3. For each class you defined above, add CSS to your theme so that the
   background-color of elements with that class is the color you want in the
   styleguide. For example,

       .styleguide-palette-css-swatch-color.bg_class_to_apply {
         background-color: #ABCDEF;
       }

   ... where #ABCDEF is the color you want to display in the styleguide.

4. Enable the new module you created.
5. Visit the style guide for your theme. The style guide will contain a section
   for each group you created, and each section will display one swatch for each
   class you defined.

KNOWN PROBLEMS
--------------

I don't know of any problems at this time, so if you find one, please let me
know by adding an issue!

VERSION HISTORY
---------------

The 1.x branch is the current stable branch.

FUTURE PLANS
------------

I don't currently have any future plans for this module: I just wanted a simple
way to output the colors I had defined / derived in SASS/Compass. That being
said, I'm open to patches so long as they don't over-complicate the module.

SIMILAR PROJECTS
----------------

As discussed in the Overview, the Style guide project contains a *Style guide
palette* module which allows a site builder to store color swatches in the
database. However, this module takes a fundamentally different approach to
achieve the same goals.

Besides *Style guide palette*, I don't know of any similar projects at this
time, so if you find one, please let me know by adding an issue!

CREDITS
-------

Concept and coding by mparker17. Sponsored by MallDatabase
(http://malldatabase.com/).
